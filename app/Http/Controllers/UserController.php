<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Entities\User;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json(UserResource::collection($users));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'email', 'max:100', 'unique:users,email'],
            'avatar' => ['nullable', 'file', 'mimes:jpeg,jpg,png', 'max:2000']
        ]);

        $uuid = Str::uuid();

        $path = '';

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $originalName = $file->getClientOriginalName();
            $path = $file->storeAs('/images/'.$uuid, $originalName);
        }

        $user = User::create([
            'uuid' => $uuid,
            'name' => $request->name,
            'email' => $request->email,
            'avatar' => $path,
        ]);

        return response()->json(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'email', 'max:100', 'unique:users,email'.$id],
            'avatar' => ['nullable', 'file', 'mimes:jpeg,jpg,png', 'max:2000']
        ]);

        $user = User::findOrFail($id);

        $update = [
            'name' => $request->name,
            'email' => $request->email,
        ];

        if ($request->hasFile('avatar')) {
            if ($user->path) {
                Storage::delete($user->path);
            }
            $file = $request->file('avatar');
            $originalName = $file->getClientOriginalName();
            $newPath = $file->storeAs('/images/'.$user->uuid, $originalName);
            $update['avatar'] = $newPath;
        }

        $result = $user->update($update);

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user =  User::findOrFail($id);
        Storage::deleteDirectory('images/'.$user->uuid);
        $user->delete();

        return response()->json(null, 204);
    }
}
