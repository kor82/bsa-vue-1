<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->getAvatarUrl(),
            'show' => true
        ];
    }


    protected function getAvatarUrl () {
        if ($this->avatar) {
            return url('storage').'/'.$this->avatar;
        }
        return $this->avatar;
    }
}
