
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit  user</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form @submit.prevent="editUser(user.id, $event)">
          <div class="form-group">
            <label for="name">Name</label>
            <input v-model="user.name" type="text" class="form-control" id="name">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input v-model="user.email" type="email" class="form-control" id="email">
          </div> 
          <div class="custom-file">
            <input @change="processFile($event)" type="file" class="custom-file-input" id="avatar">
            <label class="custom-file-label" for="avatar">@{{ avatarLabel }}</label>
          </div>        
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>
