<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Vue SPA</title>
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div id="app">

            <div class="container">

                <div class="row">                    
                    <div class="col">
                        <button class="btn btn-success shadow-sm" data-toggle="modal" data-target="#addUser">
                            Add user
                        </button>
                    </div>
                </div>

                <div class="row d-flex justify-content-start">
                    <div class="pt-2">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input v-model="search" 
                                 type="radio" 
                                 id="search-by-name" 
                                 value="name" 
                                 class="custom-control-input">
                          <label class="custom-control-label" for="search-by-name">Name</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                          <input v-model="search" 
                                 type="radio" 
                                 id="search-by-email" 
                                 value="email" 
                                 class="custom-control-input">
                          <label class="custom-control-label" for="search-by-email">Email</label>
                        </div>
                    </div>

                    <div v-if="search == 'name'" class="col-2">
                        <input type="text" 
                               class="form-control" 
                               placeholder="search by Name" 
                               @input="searchBy('name', $event)">
                    </div>

                    <div v-if="search == 'email'" class="col-2">
                        <input type="email" 
                               class="form-control" 
                               placeholder="search by Email" 
                               @input="searchBy('email', $event)">
                    </div>
                </div>   
                
                <div class="row w-75">
                    <table class="table ">
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Avatar</th>
                          <th scope="col">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="user in users"
                            :key="user.id"
                            v-show="user.show"
                        >
                          <th scope="row">@{{user.id}}</th>
                          <td>@{{user.name}}</td>
                          <td>@{{user.email}}</td>
                          <td>
                                <template v-if="user.avatar">
                                    <img class="avatar" :src="user.avatar">
                                </template>
                                <span v-else>
                                    <i class="fas fa-user-astronaut"></i>
                                </span>
                          </td>
                          <td>
                                <span @click="showEditForm(user.id)">
                                    <i class="far fa-edit"></i>
                                </span>
                                <span @click="deleteUser(user.id)">
                                    <i class="far fa-trash-alt"></i>
                                </span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>

            @include('add-user-modal')
            @include('edit-user-modal')
           
</div>
    <!--  SCRIPTS -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
