/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',

    data: {
    	users: [],
    	user: {
			id: null,
			name: 'test',
			email: 'test@test.t',
			avatar: null,
			show: true
    	},

		url: location.href.replace(/\/$/, "")+'/api/users',
    	search: 'name',
    	avatarLabel: 'Choose file',
    },

	created: function () {
		this.fetchUsers();
	},

    methods: {

    	fetchUsers: function () {
           axios.get(this.url)
                .then(response => {
                    this.users = response.data;
                })
                .catch(error => console.log(error));
           },

    	addUser: function (event) {
    		let formData = new FormData();
    		formData.append('name', this.user.name);
    		formData.append('email', this.user.email);
    		if (this.user.avatar) {
    			formData.append('avatar', this.user.avatar);
    		}
			jQuery('#addUser').modal('hide');

			axios.post( '/api/users',
			  formData,
			  {
			    headers: {
			        'Content-Type': 'multipart/form-data'
			    }
			  }
			).then( response => {
    			if (this.users.length == 0) {
    				this.users = [response.data]
    			} else {
    				this.users.push(response.data)
    			}

				this.user.id = null;
				this.user.name = '';
				this.user.email = '';
				this.user.avatar = null;
				this.avatarLabel = 'Choose file';
			})
			.catch(error => console.log(error));
    	},

    	deleteUser: function(id) {
        	axios
        		.post(this.url+'/'+id, {_method: 'DELETE'})
        		.then(response => {
					for(let i = 0; i < this.users.length; i++){
					   if (this.users[i].id === id) {
					     this.users.splice(i, 1);
					   }
					}
        		})
        		.catch(error => console.log(error))
    	},

    	editUser: function(id, event) {
    		let formData = new FormData();
    		formData.append('_method', 'PUT');
    		formData.append('name', this.user.name);
    		formData.append('email', this.user.email);
    		if (this.user.avatar) {
    			formData.append('avatar', this.user.avatar);
    		}

			jQuery('#editUser').modal('hide');

			this.users.map(user => {
				if (user.id == id) {
					user.name = this.user.name;
					user.email = this.user.email;
				}
			});

        	axios
        		.post(this.url+'/'+id, formData,
				  {
				    headers: {
				        'Content-Type': 'multipart/form-data',
				    }
				  })
        		.catch(error => console.log(error))
    	},

    	showEditForm: function(id) {
    		jQuery('#editUser').modal('show');
    		this.user.id = id;
    		let user = this.users.filter(user => user.id == id)[0];
    		this.user.name = user.name;
    		this.user.email = user.email;
    		this.user.avatar = null;
    	},

    	processFile: function (event) {
    		this.user.avatar = event.target.files[0];
    		this.avatarLabel = this.user.avatar.name;
    	},

    	searchBy: function(field, event) {
    		this.users.map(user => {
    			let input = event.target.value;
    			if (user[field].indexOf(input) < 0 && input.length > 0) {
    				user.show = false;
    			} else {
    				user.show = true;
    			}
    		});
    	}

    }
});